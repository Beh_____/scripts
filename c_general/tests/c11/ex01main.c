#include <unistd.h>
#include <stdlib.h>

int		*ft_map(int *tab, int length, int(*f)(int));

void	ft_putnbr(int a)
{
	char out;

	if (a < 0)
	{
		write(1, "-", 1);
		if (a < -9)
			ft_putnbr((a / 10) * -1);
		ft_putnbr((a % 10) * -1);
	}
	else if (a > 9)
	{
		ft_putnbr(a / 10);
		ft_putnbr(a % 10);
	}
	else
	{
		out = a + '0';
		write(1, &out, 1);	
	}
}

int	ft_puttriple(int a)
{
	return (a * 3);
}

int	ft_putnegativesquare(int a)
{
	return (-a * a);
}

int main()
{
	int arr[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	int	*res;
	int idx;
	int map_size;

	map_size = 5;
	res = ft_map(arr, map_size, &ft_puttriple);
	idx = -1;
	while (++idx < map_size)
	{
		ft_putnbr(res[idx]);
		write(1, "\n", 1);
	}
	free(res);

	map_size = 8;
	res = ft_map(arr, map_size, &ft_putnegativesquare);
	idx = -1;
	while (++idx < map_size)
	{
		ft_putnbr(res[idx]);
		write(1, "\n", 1);
	}

	int err = -1;
	res = ft_map(arr, err, &ft_puttriple);
	if (res != 0)
	{
		write(1, "FAIL\n", 5);
		free(res);
	}
}
