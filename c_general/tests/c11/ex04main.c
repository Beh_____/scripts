#include <stdio.h>

int	ft_is_sort(int *tab, int length, int (*f)(int, int));

int	reverse(int a, int b)
{
	return (a - b);
}

int	sort(int a, int b)
{
	return (b - a);
}

int main()
{
	int	arr[10] = {2, 4, 6, 8, 0, 9, 7, 5, 3, 1};
	int	arr2[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	int	arr3[10] = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
	
	printf("Expected 0, Returned %d\n", ft_is_sort(arr, 10, &reverse));
	printf("Expected 0, Returned %d\n", ft_is_sort(arr, 10, &sort));
	printf("Expected 1, Returned %d\n", ft_is_sort(arr2, 10, &reverse));
	printf("Expected 1, Returned %d\n", ft_is_sort(arr3, 10, &sort));
}
