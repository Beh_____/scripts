#include <stdio.h>

void	ft_advanced_sort_string_tab(char **tab, int (*cmp)(char *, char *));

int		ft_strcmp(char *s1, char *s2)
{
	unsigned char	c1;
	unsigned char	c2;
	unsigned int	idx;

	idx = 0;
	while (s1[idx] != '\0' || s2[idx] != '\0')
	{
		c1 = s1[idx];
		c2 = s2[idx++];
		if ((c1 - c2) != 0)
			return (c1 - c2);
	}
	return (0);
}

int		ft_rev_strcmp(char *s1, char *s2)
{
	unsigned char	c1;
	unsigned char	c2;
	unsigned int	idx;

	idx = 0;
	while (s1[idx] != '\0' || s2[idx] != '\0')
	{
		c1 = s1[idx];
		c2 = s2[idx++];
		if ((c1 - c2) != 0)
			return (c2 - c1);
	}
	return (0);
}

int main()
{
	char **p;
	printf("Sorted: \n");
	char *arr[10] = {
		"a",
		"b",
		"ab",
		"ca",
		"cd",
		"aaa",
		"aba",
		"x",
		0};
	p = arr;
	ft_advanced_sort_string_tab(p, &ft_strcmp);
	while (*p)
		printf("%s\n", *p++);

	printf("Reverse: \n");
	char *arr2[10] = {
		"a",
		"b",
		"ab",
		"ca",
		"cd",
		"aaa",
		"aba",
		"x",
		0};
	p = arr2;
	ft_advanced_sort_string_tab(p, &ft_rev_strcmp);
	while (*p)
		printf("%s\n", *p++);
}
