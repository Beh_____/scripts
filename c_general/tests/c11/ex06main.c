#include <stdio.h>

void	ft_sort_string_tab(char **tab);

int main()
{
	char *arr[10] = {
		"a",
		"b",
		"ab",
		"ca",
		"cd",
		"aaa",
		"aba",
		"x",
		0};
	char **p = arr;
	ft_sort_string_tab(p);
	while (*p)
		printf("%s\n", *p++);
}
