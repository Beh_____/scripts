#include <unistd.h>

void ft_putnbr(int nb);

int main()
{
	write(1, "Expected:945-2045621474836-748361556-214748364801042\nReturned:", 62);
	ft_putnbr(945);
	ft_putnbr(-204562);
	ft_putnbr(1474836);
	ft_putnbr(-74836);
	ft_putnbr(1556);
	ft_putnbr(-2147483648);
	ft_putnbr(0);
	ft_putnbr(10);
	ft_putnbr(42);
}
