#include <stdio.h>

int ft_atoi_base(char *str, char *base);

int main()
{
    char *s1 = "DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD";
    char *s2 = "-Dxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
    char *s3 = "--DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD";
    char *s4 = "+DDxx";
    char *b1 = "xD";
    char *b2 = "+-";
    char *b3 = " \t";
    char *b4 = "\v\r";
    char *b5 = "\f\n";
    char *b6 = "xxD";
    printf("Expected 2147483647, got %d: \n", ft_atoi_base(s1, b1));
    printf("Expected -2147483648, got %d: \n", ft_atoi_base(s2, b1));
    printf("Expected 2147483647, got %d: \n", ft_atoi_base(s3, b1));
    printf("Expected 12, got %d: \n", ft_atoi_base(s4, b1));
    printf("Expected 0, got %d: \n", ft_atoi_base(s1, b2));
    printf("Expected 0, got %d: \n", ft_atoi_base(s1, b3));
    printf("Expected 0, got %d: \n", ft_atoi_base(s1, b4));
    printf("Expected 0, got %d: \n", ft_atoi_base(s1, b5));
    printf("Expected 0, got %d: \n", ft_atoi_base(s1, b6));
}
