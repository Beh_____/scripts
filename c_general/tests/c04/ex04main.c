#include <unistd.h>

void ft_putnbr_base(int nbr, char *base);

int main()
{
    int max = 2147483647;
    int min = -2147483648;
    int normal = 10;
    char *base1 = "1";
    char *base2 = "lO";
    char *base3 = "repeated";
    char *base4 = "\n\t ";

    write(1, "\nExpected , got:", 16);
    ft_putnbr_base(max, base1);
    write(1, "\nExpected:OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO\nReturned:", 51);
    ft_putnbr_base(max, base2);
    write(1, "\nExpected:-Olllllllllllllllllllllllllllllll\nReturned:", 53);
    ft_putnbr_base(min, base2);
    write(1, "\nExpected , got:", 16);
    ft_putnbr_base(min, base3);
    write(1, "\nExpected l, got:", 17);
    ft_putnbr_base(0, base2);
    write(1, "\nExpected \t\n\t, got:", 19);
    ft_putnbr_base(normal, base4);
    write(1, "\nExpected something, got:", 25);
    ft_putnbr_base(max, base4);
    
}
