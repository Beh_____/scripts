// cat -e does not play well when mixing output from printf and write
#include <unistd.h>

void ft_putstr_non_printable(char *str);

void putstr(char *c, int n)
{
    for(int i = 0; i < n; i++)
    {
        write(1, &c[i], 1);
    }
}

int main()
{
    char output1[] = "String: ";
    char output2[] = "\nFunction output: ";
    char newline[] = "\n";
    // Provided example
    char *str = "Coucou\ntu";
    putstr(output1, 8);
    putstr(str, 9);
    putstr(output2, 18);
    ft_putstr_non_printable(str);
    putstr(newline, 1);

    // Boundary testing
    str = "\x01\x1F\x7F\x8F\xFF";
    putstr("String: ", 8);
    putstr(str, 5);
    putstr("\nFunction output: ", 18);
    ft_putstr_non_printable(str);
    putstr("\n", 1);
}
