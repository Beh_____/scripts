#include <stdio.h>

void *ft_print_memory(void *addr, unsigned int size);

int main()
{
    char *contents = "Do what you want cause a pirate is free, you are a pirate!";
	char *contents2 = "\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\x20\x7e\x7f\x80\xfe\xff";
	char *contents3 = "Short";

    void *addr = contents;
	printf("Returns: %p\n", ft_print_memory(addr, 58));
	addr = contents2;
	printf("Returns: %p\n", ft_print_memory(addr, 38));
	addr = contents3;
	printf("Returns: %p\n", ft_print_memory(addr, 6));
    return 0;
}
