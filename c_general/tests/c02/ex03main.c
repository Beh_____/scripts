#include <stdio.h>

int ft_str_is_numeric(char *str);

int main()
{
    char *str = "test1";
    printf("Expected 0, returned %d\n", ft_str_is_numeric(str));

    str = "/";
    printf("Expected 0, returned %d\n", ft_str_is_numeric(str));
    
    str = ":";
    printf("Expected 0, returned %d\n", ft_str_is_numeric(str));

    str = "1234";
    printf("Expected 1, returned %d\n", ft_str_is_numeric(str));

    str = "";
    printf("Expected 1, returned %d\n", ft_str_is_numeric(str));
}
