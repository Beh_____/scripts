#include <stdio.h>

int ft_str_is_alpha(char *str);

int main()
{
    char *str = "test1";
    printf("Expected 0, returned %d\n", ft_str_is_alpha(str));

    str = "@";
    printf("Expected 0, returned %d\n", ft_str_is_alpha(str));

    str = "[";
    printf("Expected 0, returned %d\n", ft_str_is_alpha(str));

    str = "`";
    printf("Expected 0, returned %d\n", ft_str_is_alpha(str));

    str = "{";
    printf("Expected 0, returned %d\n", ft_str_is_alpha(str));

    str = "ATESTZ";
    printf("Expected 1, returned %d\n", ft_str_is_alpha(str));

    str = "atestz";
    printf("Expected 1, returned %d\n", ft_str_is_alpha(str));

    str = "";
    printf("Expected 1, returned %d\n", ft_str_is_alpha(str));
}
