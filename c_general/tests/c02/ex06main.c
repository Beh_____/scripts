#include <stdio.h>

int ft_str_is_printable(char *str);

int main()
{
    char *str = "TEST low3r case []";
    printf("Expected 1, returned %d\n", ft_str_is_printable(str));

    str = "\x1F";
    printf("Expected 0, returned %d\n", ft_str_is_printable(str));

    str = "\x7F";
    printf("Expected 0, returned %d\n", ft_str_is_printable(str));

    str = "\n\r";
    printf("Expected 0, returned %d\n", ft_str_is_printable(str));

    str = " ~";
    printf("Expected 1, returned %d\n", ft_str_is_printable(str));

    str = "";
    printf("Expected 1, returned %d\n", ft_str_is_printable(str));
}
