#include <stdio.h>
#include <stdlib.h>

char	*ft_strdup(char *s);

int main()
{
	char *s1 = "edited";
	char *s2 = "";

	char *s3 = ft_strdup(s1);
	char *s4 = ft_strdup(s2);
	*(s3) = 'E';
	printf("Expected Edited, Returned %s\n", s3);
	printf("Expected edited, Returned %s\n", s1);
	printf("Expected , Returned %s", s4);

	free(s3);
	free(s4);
}
