#include <unistd.h>

void    ft_ultimate_div_mod(int *a, int *b);

void    ft_print_number(int nb, int n_lead_zero)
{
    long    l_nb;
    int     nb_p1;
    int     nb_p2;
    char    ch;

l_nb = nb;
    if (l_nb < 0)
    {
write(1, "-", 1);
l_nb = l_nb * -1;
    }
    nb_p1 = l_nb / 10;
    nb_p2 = l_nb % 10;
    if (nb_p1 > 0 || n_lead_zero > 1)
    {
ft_print_number(nb_p1, --n_lead_zero);
    }
    ch = '0' + nb_p2;
    write(1, &ch, 1);
}

void    test_ultimate_div_mod(int nb, int base)
{
    int n_div;
int n_mod;

n_div = nb;
    n_mod = base;
    ft_ultimate_div_mod(&n_div, &n_mod);
    ft_print_number(nb, 0);
    write(1, " div / mod ", 11);
    ft_print_number(base, 0);
    write(1, " is ", 4);
    ft_print_number(n_div, 0);
    write(1, " & ", 3);
    ft_print_number(n_mod, 0);
    write(1, "\n", 1);
}

int     main(void)
{
    test_ultimate_div_mod(42, 10);
    test_ultimate_div_mod(42, 3);
    test_ultimate_div_mod(42, 5);
    test_ultimate_div_mod(42, 7);
    test_ultimate_div_mod(42, 11);
    test_ultimate_div_mod(42, 13);
    test_ultimate_div_mod(42, 17);
    test_ultimate_div_mod(42, 19);
    return (0);
}
