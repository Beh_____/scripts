#include <unistd.h>

int     ft_strlen(char *str);

void    ft_print_number(int nb, int n_lead_zero)
{
    long    l_nb;
    int     nb_p1;
    int     nb_p2;
    char    ch;

l_nb = nb;
    if (l_nb < 0)
    {
write(1, "-", 1);
l_nb = l_nb * -1;
    }
    nb_p1 = l_nb / 10;
    nb_p2 = l_nb % 10;
    if (nb_p1 > 0 || n_lead_zero > 1)
    {
ft_print_number(nb_p1, --n_lead_zero);
    }
    ch = '0' + nb_p2;
    write(1, &ch, 1);
}

int     main(void)
{
    int len;

len = ft_strlen("Exercise 06: ft_strlen - by 42KL Team. 47 vs 47");
    write(1, "Exercise 06: ft_strlen - by 42KL Team. 47 vs ", 45);
    ft_print_number(len, 0);
    return (0);
}
