#include <stdio.h>
#include <unistd.h>

void ft_putnbr(int n);

int main()
{
    ft_putnbr(-2147483648); 
    write(1, "\n", 1);
    ft_putnbr(-12345671); 
    write(1, "\n", 1);
    ft_putnbr(-4); 
    write(1, "\n", 1);
    ft_putnbr(0); 
    write(1, "\n", 1);
    ft_putnbr(9); 
    write(1, "\n", 1);
    ft_putnbr(10); 
    write(1, "\n", 1);
    ft_putnbr(12345678); 
    write(1, "\n", 1);
    ft_putnbr(214743647);
}
